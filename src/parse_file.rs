use nom::{rest, types::CompleteStr};


type AttributeList = Vec<(String, Option<String>)>;

#[derive(Debug)]
pub enum ConditionalKind { If, ElseIf, Else, Unless }

enum NodeData {
  Tag { tagname: String, attributes: Option<AttributeList> },
  Filter { name: String, attributes: Option<AttributeList> },
  Code { buffered: bool },
  ConditionalStatement { kind: ConditionalKind, expression: String },
  MixinDeclaration { name: String, args: Vec<String> }
}

enum LeafData {
  Text(String),
  Doctype(String),
  Comment { buffered: bool, text: String },
  MixinCall { name: String, args: Vec<String> }
}

enum FileNode {
  Node{ data: NodeData, children: Vec<FileNode>),
  Leaf{ data: LeafData }
}
