use nom::{rest, types::CompleteStr};

// TODO: Iterators, Interpolation, Includes, Inheritance
// All I's for some reason


#[derive(Debug)]
pub enum Indentation {
    Tabs(usize),
    Spaces(usize),
}

#[derive(Debug)]
pub struct Line {
    indentation: Option<Indentation>,
    line_type: LineType,
}

#[derive(Debug)]
pub enum LineType {
    CodeLine(LineContents),
    TextLine(String),
}

named!(parse_indentation<CompleteStr, Option<Indentation> >,
    opt!(
        alt!(
            many1_count!(tag!("\t")) => { |l| Indentation::Tabs(l) } |
            many1_count!(tag!(" ")) => { |l| Indentation::Spaces(l) }
        )
    )
);

named!(parse_line<CompleteStr, Line>,
    do_parse!(
        indentation: parse_indentation >>
        contents: parse_line_content >>
        (Line { indentation, line_type: LineType::CodeLine(contents) })
    )
);

/*
parse_code_line(indent_level):
    parse_indent
    code = parse_code
    if has block:
        if code block:
            many: parse_code_line(indent_level + 1)
        if text block:
            many: parse_text_line(indent_level + 1)
    else:
        return code

parse_text_line(indent_level):
    parse_indent
    return rest

parse_file:
    many:
        parse_code_line:
            

*/

/*

struct Node {

}

trait Node {
    children
}

named!(parse_file<CompleteStr, Vec<Block>>, many0!(parse_code_line));

named!(parse_code_line(indent_level: usize)<CompleteStr, Node>, 
    do_parse!(
        indent: parse_indent >>
        code: parse_code >>
        child: switch!(code.block_type().is_some(),
            true => switch!(code.block_type(),
                CodeBlock => many1!(apply!(parse_code_line, indent_level + 1)) |
                TextBlock => many1!(apply!(parse_text_line, indent_level + 1))
            )
            false => map!(rest, 
        ) >>
        (Block {

        }) 
    )
);

named!(parse_text_line(indent_level: usize)<CompleteStr, String>)

*/

#[derive(Debug)]
pub enum LineContents {
    Doctype { text: String },
    Tag { tagname: String, attributes: Option<Vec<(String, Option<String>)>> },
    Comment { buffered: bool, text: String },
    Filter { name: String, options: Option<String> },
    Code { buffered: bool, text: Option<String> },
    ConditionalStatement { kind: ConditionalType, expression: String },
    MixinDeclaration { name: String, args: Option<Vec<String>> },
    MixinCall { name: String, args: Option<Vec<String>> },
}

named!(pub parse_line_content<CompleteStr, LineContents>,
    alt!(parse_doctype | parse_conditional | parse_mixin_declaration |
        parse_mixin_call | parse_tag | parse_comment | parse_filter |
        parse_code ));


named!(valid_name<CompleteStr, CompleteStr>,
    take_while1!(|c| match c {
        'A'...'Z' => true,
        'a'...'z' => true,
        '0'...'9' => true,
        '_' | '-' => true,
        _ => false
    })
);

named!(white_space<CompleteStr, CompleteStr>,
    take_while1!(|c| match c {
        ' ' | '\t' => true,
        _ => false,
    })
);

named!(quote_string<CompleteStr, CompleteStr>,
    delimited!(
        tag!("\""),
        escaped!(
            take_while1!(|c| match c {
                '\\' | '"' | '\n' => false,
                _ => true,
            }),
            '\\',
            alt!(
                tag!("\\") |
                tag!("\"")
            )
        ),
        tag!("\"")
    )
);

named!(attr_list<CompleteStr, Option<Vec<(String, Option<String>)>>>,
    opt!(
        separated_list!(
            tag!(" "),
            alt!(
                ws!(separated_pair!(
                    valid_name,
                    tag!("="),
                    alt!(valid_name | quote_string),
                )) => { |(n, v)| (n.to_string(), Some(v.to_string()))} |
                valid_name => { |n: CompleteStr| (n.to_string(), None) }
            )
        )
    )
);


named!(parse_tag<CompleteStr, LineContents>,
    do_parse!(
        tagname: valid_name >>
        attributes: opt!(delimited!(tag!("("), attr_list, tag!(")"))) >>
        (LineContents::Tag {
            tagname: tagname.to_string(),
            attributes: match attributes {
                Some(n) => n,
                None => None
            }
        })
    )
);

named!(parse_comment<CompleteStr, LineContents>,  
    do_parse!(
        tag!("//") >>
        buffered: opt!(tag!("-")) >>
        text: rest >>
        (LineContents::Comment {
            buffered: buffered.is_some(),
            text: text.to_string()
        })
    )
);

named!(parse_doctype<CompleteStr, LineContents>,
    do_parse!(
        tag!("doctype ") >>
        text: rest >>
        (LineContents::Doctype { text: text.to_string() })
    )
);

named!(parse_filter<CompleteStr, LineContents>,
    do_parse!(
        tag!(":") >>
        name: valid_name >>
        options: opt!(delimited!(tag!("("), take_until!(")"), tag!(")"))) >>
        (LineContents::Filter {
            name: name.to_string(),
            options: options.map(|s| s.to_string())
        })
    )
);

named!(parse_code<CompleteStr, LineContents>,
    do_parse!(
        buffered: alt!(
            value!(false, tag!("-")) |
            value!(true, tag!("="))
        ) >>
        text: opt!(preceded!(opt!(tag!(" ")), rest)) >>
        (LineContents::Code {
            buffered,
            text: text.map(|s| s.to_string())
        })
    )
);

#[derive(Debug)]
pub enum ConditionalType { If, ElseIf, Else, Unless }

named!(parse_conditional<CompleteStr, LineContents>,
    do_parse!(
        kind: alt!(
            value!(ConditionalType::If, tag!("if")) |
            value!(ConditionalType::ElseIf, tag!("else if")) |
            value!(ConditionalType::Else, tag!("else")) |
            value!(ConditionalType::Unless, tag!("unless"))
        ) >>
        tag!(" ") >>
        expression: rest >>
        (LineContents::ConditionalStatement{
            kind, expression: expression.to_string() })
    )
);

named!(parse_mixin_declaration<CompleteStr, LineContents>,
    do_parse!(
        tag!("mixin ") >>
        name: valid_name >>
        args: opt!(delimited!(
            tag!("("),
            opt!(ws!(separated_list!(tag!(","), take_until!(")")))),
            tag!(")")
        )) >>
        (LineContents::MixinDeclaration {
            name: name.to_string(),
            args: match args {
                Some(v) => v.map(|v| v.iter().map(|s| s.to_string()).collect()),
                None => None
            }
        })
    )
);

named!(parse_mixin_call<CompleteStr, LineContents>,
    do_parse!(
        tag!("+") >>
        name: valid_name >>
        args: opt!(delimited!(
            tag!("("),
            opt!(ws!(separated_list!(tag!(","), take_until!(")")))),
            tag!(")")
        )) >>
        (LineContents::MixinCall {
            name: name.to_string(),
            args: match args {
                Some(v) => v.map(|v| v.iter().map(|s| s.to_string()).collect()),
                None => None
            }
        })
    )
);
